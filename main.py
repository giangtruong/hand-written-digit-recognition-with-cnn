import pickle
import sys
from keras.datasets import mnist
from Train import train as tr
import numpy as np 
import cv2
import keras

def load_model():
    model_path = "Model"
    model_name = "default_model"
    json_file = open(model_path + "/" + model_name + ".json", "r")
    loaded_model_json = json_file.read()
    json_file.close()
    model = keras.models.model_from_json(loaded_model_json)
    model.load_weights(model_path + "/" + model_name + ".h5")

    return model

def main():
    model = load_model()

    filepath = "images/img10.png"
    img = cv2.imread(filepath, 0)
    cv2.imshow("origin image", img)

    cv2.threshold(img, 30, 255, cv2.THRESH_BINARY, img)
    img = cv2.resize(img, (28, 28))
    img = img.reshape(1, 28, 28)
    img = img / 255

    test = np.asarray([img], dtype = "float32")

    result = model.predict(test)
    print(result)
    result = np.where(result[0] == np.amax(result[0]))
    
    print(result[0])
    print()
    cv2.waitKey(0)

if __name__ == "__main__":
    main()