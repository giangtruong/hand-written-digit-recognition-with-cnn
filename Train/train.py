from keras.datasets import mnist
import matplotlib.pyplot as plt
import pickle 

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
K.set_image_dim_ordering('th')

def test():
    (X_train, Y_train), (X_test, Y_test) = mnist.load_data()
    
    plt.subplot(222)
    plt.imshow(X_train[0], cmap = plt.get_cmap('gray'))
    plt.subplot(222)
    plt.imshow(X_train[1], cmap = plt.get_cmap('gray'))
    plt.subplot(223)
    plt.imshow(X_train[2], cmap = plt.get_cmap('gray'))
    plt.subplot(224)
    plt.imshow(X_train[3], cmap = plt.get_cmap('gray'))

    plt.show()

def load_data():
    (X_train, Y_train), (X_test, Y_test) = mnist.load_data()
    return X_train, Y_train, X_test, Y_test

def vectorization(X_train, X_test):
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train = X_train.reshape(X_train.shape[0], 1, 28, 28).astype('float32')
    X_test = X_test.reshape(X_test.shape[0], 1, 28, 28).astype('float32')
    return X_train, X_test

def normalization(X_train, X_test):
    X_train /= 255
    X_test /= 255
    return X_train, X_test

def categorize_class(Y_train, Y_test):
    Y_train = np_utils.to_categorical(Y_train)
    Y_test = np_utils.to_categorical(Y_test)

    return Y_train, Y_test

def base_models(num_parameters, num_classes):
    model = Sequential()
    model.add(Conv2D(30, (5, 5), input_shape = (1, 28, 28), activation = "relu"))
    model.add(MaxPooling2D(pool_size = (2, 2)))
    model.add(Conv2D(15, (3, 3), activation = "relu"))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation = "relu"))
    model.add(Dense(50, activation = "relu"))
    model.add(Dense(num_classes, activation = "softmax"))

    model.compile(loss = "categorical_crossentropy", optimizer = "adam", metrics = ["accuracy"])
    return model

def train():
    #seed = 7
    #np.random.seed(seed)

    X_train, Y_train, X_test, Y_test = load_data()
    X_train, X_test = vectorization(X_train, X_test)
    X_train, X_test = normalization(X_train, X_test)
    Y_train, Y_test = categorize_class(Y_train, Y_test)

    model = base_models(X_train.shape[1], Y_train.shape[1])

    model.fit(  X_train, Y_train, validation_data = (X_test, Y_test), epochs = 10, batch_size = 1024, verbose = 1)
    scores = model.evaluate(X_test, Y_test, verbose = 1)
    print("Baseline Error: %.2f%%" % (100-scores[1]*100))   

    filename = "Model/model1"
    model_json = model.to_json()    
    with open(filename + ".json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights(filename + ".h5")
    print("Save model to disk")



if __name__ == "__main__":
    train()

